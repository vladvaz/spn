// authors route
const couch = require("../DB");
const express = require("express");
const authorsRoutes = express.Router();

// get all authors
authorsRoutes.get("/", auth, async (req, res) => {
  const DB = couch.use("d_authors");
  try {
    const docs = await DB.list({ include_docs: true });
    res.send(docs.rows[0].doc);
  } catch (err) {
    res.send(err.message);
  }
});

module.exports = authorsRoutes;
