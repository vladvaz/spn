// user route
const bcrypt = require("bcrypt");
const config = require("config");
const couch = require("../DB");
const jwt = require("jsonwebtoken");
const express = require("express");
const usersRoutes = express.Router();

// get session permission
usersRoutes.post("/auth", async (req, res) => {
  try {
    const DB = couch.use("d_users");
    const doc = await DB.get(req.body._id);
    const validPassword = await bcrypt.compare(req.body.password, doc.password);

    if (!validPassword) return res.status(400).send("Invalid ID or password");

    delete doc["password"];
    const token = jwt.sign(
      { _id: doc._id, name: doc.name, pkh: doc.pkh },
      config.get("jwtPrivateKey")
    );
    let obj = {
      ...doc,
      spn_token: token
    };
    res.send(obj);
  } catch (err) {
    res.status(400).send("Invalid ID or password");
  }
});

// get user data
usersRoutes.get("/:id", async (req, res) => {
  const DB = couch.use("d_users");
  console.log(req.params.id);

  try {
    const doc = await DB.get(req.params.id);
    delete doc["username"];
    delete doc["password"];
    res.send(doc);
  } catch (err) {
    res.send(err.message);
  }
});

module.exports = usersRoutes;
