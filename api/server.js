// server.js
const bodyParser = require("body-parser");
const config = require("config");
const express = require("express");
const app = express();
const PORT = 4000;
const articlesRoute = require("./routes/articles");
const authorsRoute = require("./routes/authors");
const commentsRoute = require("./routes/comments");
const usersRoute = require("./routes/users");

if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwtPrivateKey is not defined!");
  process.exit(1);
}

if (!config.get("couchPrivateKey")) {
  console.error("FATAL ERROR: couchPrivateKey is not defined!");
  process.exit(1);
}

app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "100mb" }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "spn_token, Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use("/articles", articlesRoute);
app.use("/authors", authorsRoute);
app.use("/comments", commentsRoute);
app.use("/users", usersRoute);

app.listen(PORT, function() {
  console.log("Server is running on Port:", PORT);
});
