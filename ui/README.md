# **DApp SPN - Scientific Publishing Network**

Este projeto foi criado utilizando o código _boilerplate_ [Create React App](https://github.com/facebook/create-react-app).

## Correr a UI da DApp

É necessário ter a `API` e a `sandbox` a correr antes de executar estes passos. A comunicação com a `sandbox` [Granary](https://github.com/stove-labs/granary/tree/master) é feita através do endereço: `http://localhost:18731`.

```
cd ui
npm install
npm start
```

A DApp corre em mode de desenvolvimento. Utilizar um editor para modificar o código (e.g.: VSCode).<br>
Abrir o URL [http://localhost:3000](http://localhost:3000) no navegador. O navegador recomendado é o [Mozilla Firefox](https://www.mozilla.org/pt-PT/firefox/).<br> Para evitar avisos de `CORS` utilizar a extensão `CORS Everywhere`.

## Para criar uma versão otimizada para correr num servidor, fazer:

```
npm run build
```
