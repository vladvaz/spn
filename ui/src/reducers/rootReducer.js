const initState = {
  isLogged: sessionStorage.getItem("isLogged") === "true"
};

const rootReducer = (state = initState, action) => {
  if (action.type === "UPDATE_STATE") {
    console.log("Update", action.new_state.isLogged);

    return {
      isLogged: action.new_state.isLogged
    };
  }
  return state;
};

export default rootReducer;
