import React, { Component } from "react";
import AddArticle from "./components/AddArticle";
import AddReview from "./components/AddReview";
import AddVersion from "./components/AddVersion";
import AddComment from "./components/AddComment";
import NavBar from "./components/NavBar.js";
import Home from "./components/Home.js";
import ListFiles from "./components/ListFiles.js";
import About from "./components/About.js";
import Detail from "./components/Detail.js";
import Search from "./components/Search.js";
import User from "./components/User.js";
import Login from "./components/Login";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";

class App extends Component {
  state = {
    logged: false
  };

  componentDidMount() {
    if (sessionStorage.getItem("isLogged") === "true") {
      this.setState({
        logged: true
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLogged !== this.props.isLogged) {
      this.setState({
        logged: !this.state.logged
      });
    }
  }

  isLoggedIn = () => {
    if (this.state.logged) {
      return (
        <div>
          <Route exact path="/" component={Home} />
          <Route exact path="/addfile" component={AddArticle} />
          <Route exact path="/addreview" component={AddReview} />
          <Route exact path="/addversion" component={AddVersion} />
          <Route exact path="/addcomment" component={AddComment} />
        </div>
      );
    } else {
      return <Route exact path="/" component={Login} />;
    }
  };

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <NavBar isLogged={this.state.logged} />
          <p />
          {this.isLoggedIn()}
          <Route exact path="/about" component={About} />
          <Route exact path="/listfiles" component={ListFiles} />
          <Route exact path="/article/:id" component={Detail} />
          <Route exact path="/search/:params" component={Search} />
          <Route exact path="/user/:id" component={User} />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogged: state.isLogged
  };
};

export default connect(mapStateToProps)(App);
