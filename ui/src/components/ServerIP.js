const ip = "127.0.0.1";
const port = "4000";
const protocol = "http";

const url = `${protocol}://${ip}:${port}`;

export default url;
