import React from "react";
import Alert from "react-bootstrap/Alert";

function HeaderBar(props) {
  const title = props.header;
  return (
    <div>
      <Alert
        style={{
          backgroundColor: "#17a2b8",
          color: "white",
          textAlign: "center"
        }}
      >
        <Alert.Heading>{title}</Alert.Heading>
      </Alert>
      <p />
    </div>
  );
}

export default HeaderBar;
