import React, { Component } from "react";
import ListFilesTemplate from "./ListFilesTemplate";
import ListGroup from "react-bootstrap/ListGroup";
import HeaderBar from "./HeaderBar";
import axios from "axios";
import { connect } from "react-redux";
import url from "./ServerIP";
import Alert from "react-bootstrap/Alert";

class ListFiles extends Component {
  state = {
    articles: [],
    alert: false
  };

  getUserData = () => {
    let id = this.props.location.state.id;

    axios
      .get(url + "/users/" + id)
      .then(response => {
        if (this.props.location.state.type === "articles") {
          this.getDocs(response.data.articles);
        }

        if (this.props.location.state.type === "reviews") {
          this.getDocs(response.data.reviews);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  getDocs = articles => {
    axios
      .post(url + "/articles/docs/multi", {
        articles: articles
      })
      .then(response => {
        if (response.data.rows.length === 0) this.setState({ alert: true });
        this.setState({ articles: response.data.rows });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  componentDidMount() {
    this.setState({ alert: false });
    this.getUserData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({ alert: false });
      this.getUserData();
    }
  }

  handleDismiss = () => {
    this.setState({
      alert: false
    });
  };

  showAlert = () => {
    if (this.state.alert === true) {
      return (
        <div>
          <p />
          <Alert variant="warning" onClose={this.handleDismiss} dismissible>
            No articles found.
          </Alert>
        </div>
      );
    }
  };

  render() {
    return (
      <div>
        <HeaderBar header={"Results List"} />
        {this.showAlert()}
        <ListGroup>
          {this.state.articles.map(article => (
            <ListFilesTemplate
              key={article.doc._id}
              id={article.doc._id}
              title={article.doc.title}
              authors={article.doc.authors}
              tags={article.doc.tags}
              reviews={article.doc.reviews}
            />
          ))}
        </ListGroup>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_id: state.user_id
  };
};

export default connect(mapStateToProps)(ListFiles);
