import React, { Component } from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import { Link } from "react-router-dom";

let badgeMargin = {
  marginRight: "4px"
};

class ListFilesTemplate extends Component {
  getAttach = attachments => {
    const id = this.props.id;
    const name = Object.keys(attachments);
    const link = "http://localhost:5984/articles/" + id + "/" + name[0];
    return (
      <p>
        <Badge variant="info" style={badgeMargin}>
          <strong>File: </strong>
        </Badge>
        <a href={link}>{name}</a>
      </p>
    );
  };

  getLink = (searchType, seachKey) => {
    let s = searchType + '={"keywords":["' + seachKey + '"]}';
    return s;
  };

  render() {
    const authors = this.props.authors.map((author, i) => {
      return (
        <Link key={i} to={"/user/" + author.id}>
          <Badge variant="secondary" style={badgeMargin}>
            {author.name}
          </Badge>
        </Link>
      );
    });

    const tags = this.props.tags.map((tag, i) => {
      return (
        <Link key={i} to={"/search/" + this.getLink("tag", tag)}>
          <Badge variant="secondary" style={badgeMargin}>
            {tag}
          </Badge>
        </Link>
      );
    });

    const titleReview =
      this.props.reviews.length > 0 ? (
        <Badge variant="warning" style={badgeMargin}>
          <strong>Reviews: </strong>
        </Badge>
      ) : (
        <div />
      );

    const reviews =
      this.props.reviews.length > 0 ? (
        this.props.reviews.map((review, i) => {
          return (
            <Link key={i} to={"/article/" + review.id}>
              <Badge variant="secondary" style={badgeMargin}>
                {review.score} ({review.expertise})
              </Badge>
            </Link>
          );
        })
      ) : (
        <div />
      );

    return (
      <ListGroup.Item>
        <Container className="fluid" style={{ maxWidth: "100%" }}>
          <Row>
            <Col>
              <div>
                <Badge variant="info" style={badgeMargin}>
                  <strong>Title: </strong>
                </Badge>
                <strong>{this.props.title}</strong>
                <p style={{ marginBottom: "0px" }} />
                <Badge variant="info" style={badgeMargin}>
                  <strong>Authors: </strong>
                </Badge>
                {authors}
                <p style={{ marginBottom: "0px" }} />
                <Badge variant="info" style={badgeMargin}>
                  <strong>Keywords: </strong>
                </Badge>
                {tags}
                <p style={{ marginBottom: "0px" }} />
                {titleReview}
                {reviews}
              </div>
            </Col>
            <Col md="auto" style={{ display: "flex", alignItems: "center" }}>
              <Button
                variant="info"
                as={Link}
                to={"/article/" + this.props.id}
                type=""
              >
                See Details
              </Button>
            </Col>
          </Row>
        </Container>
      </ListGroup.Item>
    );
  }
}

export default ListFilesTemplate;
