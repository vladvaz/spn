import React, { Component } from "react";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";

let badgeButPadd = {
  padding: "0rem 0.3rem",
  fontSize: "0.7rem"
};

let badgeMar = {
  marginLeft: "5px",
  marginRight: "5px",
  marginTop: "5px",
  marginBottom: "5px"
};

class Tags extends Component {
  render() {
    const res = this.props.tags.map(tag => {
      return (
        <Badge key={tag} variant="secondary" style={badgeMar}>
          {tag}{" "}
          <Button
            id={tag}
            variant="light"
            size="sm"
            style={badgeButPadd}
            onClick={this.props.eraseTag}
          >
            x
          </Button>
        </Badge>
      );
    });

    return <div>{res}</div>;
  }
}

export default Tags;
