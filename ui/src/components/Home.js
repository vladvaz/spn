import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import Form from "react-bootstrap/Form";
import axios from "axios";
import { connect } from "react-redux";
import url from "./ServerIP";

let styleLabel = {
  color: "Black"
};

class Home extends Component {
  state = {
    _id: "",
    name: "",
    pkh: "",
    email: [],
    employment: [],
    articles: [],
    reviews: [],
    n_articles: "",
    n_reviews: ""
  };

  componentDidMount() {
    this.getUserData();
  }

  getUserData = () => {
    let id = sessionStorage.getItem("_id");

    axios
      .get(url + "/users/" + id)
      .then(response => {
        let n_art = response.data.articles.length;
        let n_rev = response.data.reviews.length;

        this.setState({
          _id: response.data._id,
          name: response.data.name,
          pkh: response.data.pkh,
          email: response.data.email,
          employment: response.data.employment,
          articles: response.data.articles,
          reviews: response.data.reviews,
          n_articles: n_art,
          n_reviews: n_rev
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  renderArray = e => {
    return e.map((element, i) => {
      return (
        <p key={i}>
          <Form.Control type="text" readOnly value={element} />{" "}
        </p>
      );
    });
  };

  render() {
    return (
      <div>
        <HeaderBar header={"My Data"} />

        <Form>
          <Form.Group controlId="name">
            <Form.Label style={styleLabel}>Name:</Form.Label>
            <Form.Control type="text" readOnly value={this.state.name} />
          </Form.Group>

          <Form.Group controlId="_id">
            <Form.Label style={styleLabel}>Orcid ID:</Form.Label>
            <Form.Control type="text" readOnly value={this.state._id} />
          </Form.Group>

          <Form.Group controlId="pkh">
            <Form.Label style={styleLabel}>Tezos public key hash:</Form.Label>
            <Form.Control type="text" readOnly value={this.state.pkh} />
          </Form.Group>

          <Form.Group controlId="email">
            <Form.Label style={styleLabel}>E-mails:</Form.Label>
            {this.renderArray(this.state.email)}
          </Form.Group>

          <Form.Group controlId="employment">
            <Form.Label style={styleLabel}>Employments:</Form.Label>
            {this.renderArray(this.state.employment)}
          </Form.Group>

          <Form.Group controlId="n_articles">
            <Form.Label style={styleLabel}>Articles Submitted:</Form.Label>
            <Form.Control type="text" readOnly value={this.state.n_articles} />
          </Form.Group>

          <Form.Group controlId="n_reviews">
            <Form.Label style={styleLabel}>Articles Reviewed:</Form.Label>
            <Form.Control type="text" readOnly value={this.state.n_reviews} />
          </Form.Group>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogged: state.isLogged
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateState: new_state => {
      dispatch({ type: "UPDATE_STATE", new_state: new_state });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
