import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import axios from "axios";
import Container from "react-bootstrap/Container";
import Badge from "react-bootstrap/Badge";
import CommentDetails from "./CommentDetails";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import url from "./ServerIP";

let badgeMargin = {
  marginRight: "4px"
};

class Detail extends Component {
  state = {
    _id: "",
    _rev: "",
    reviewed_article_id: "",
    title: "",
    authors: [],
    tags: [],
    article_abstract: "",
    article_version: "",
    next_version: "",
    previous_version: "",
    reviews: [],
    comments: [],
    review_score: "",
    expertise_score: "",
    hash_contract: "",
    submitter_pkh: "",
    _attachments: "",
    date: ""
  };

  getArticleData = () => {
    let id = this.props.match.params.id;

    axios
      .get(url + "/articles/docs/" + id)
      .then(response => {
        this.setState({
          _id: response.data._id,
          _rev: response.data._rev,
          reviewed_article_id: response.data.reviewed_article_id,
          title: response.data.title,
          authors: response.data.authors,
          tags: response.data.tags,
          article_abstract: response.data.article_abstract,
          article_version: response.data.article_version,
          next_version: response.data.next_version,
          previous_version: response.data.previous_version,
          reviews: response.data.reviews,
          comments: response.data.comments,
          review_score: response.data.review_score,
          expertise_score: response.data.expertise_score,
          hash_contract: response.data.hash_contract,
          submitter_pkh: response.data.submitter_pkh,
          _attachments: response.data._attachments,
          date: response.data.date
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  componentDidMount() {
    this.getArticleData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.getArticleData();
    }
  }

  getAttach = () => {
    const id = this.state._id;
    const name = Object.keys(this.state._attachments);
    const link = "http://localhost:5984/d_articles/" + id + "/" + name[0];
    return (
      <p>
        <Badge variant="info" style={badgeMargin}>
          <strong>File: </strong>
        </Badge>
        <a href={link}>
          <Badge variant="secondary"> {name}</Badge>
        </a>
      </p>
    );
  };

  getLink = (searchType, seachKey) => {
    let s = searchType + '={"keywords":["' + seachKey + '"]}';
    return s;
  };

  authors = a => {
    return a.map((author, i) => {
      return (
        <Link key={i} to={"/user/" + author.id}>
          <Badge variant="secondary" style={badgeMargin}>
            {author.name}
          </Badge>
        </Link>
      );
    });
  };

  tags = t => {
    return t.map((tag, i) => {
      return (
        <Link key={i} to={"/search/" + this.getLink("tag", tag)}>
          <Badge variant="secondary" style={badgeMargin}>
            {tag}
          </Badge>
        </Link>
      );
    });
  };

  reviews = r => {
    return r.map((rev, i) => {
      return (
        <Link key={i} to={"/article/" + rev.id}>
          <Badge variant="secondary" style={badgeMargin}>
            {rev.score} ({rev.expertise})
          </Badge>
        </Link>
      );
    });
  };

  titleReview = () => {
    if (this.state.reviewed_article_id !== "") {
      return (
        <div>
          <Badge variant="warning" style={badgeMargin}>
            <strong>Article reviewed: </strong>
          </Badge>
          <Link to={"/article/" + this.state.reviewed_article_id}>
            <Badge variant="secondary" style={badgeMargin}>
              <strong>{this.state.reviewed_article_id}</strong>
            </Badge>
          </Link>
          <p />
          <Badge variant="warning" style={badgeMargin}>
            <strong>Score: </strong>
          </Badge>
          <Badge variant="secondary" style={badgeMargin}>
            <strong>{this.state.review_score}</strong>
          </Badge>
          <p />
          <Badge variant="warning" style={badgeMargin}>
            <strong>Expertise: </strong>
          </Badge>
          <Badge variant="secondary" style={badgeMargin}>
            <strong>{this.state.expertise_score}</strong>
          </Badge>
          <p />
        </div>
      );
    } else {
      return null;
    }
  };

  reviewsLinks = () => {
    if (this.state.reviewed_article_id === "") {
      return (
        <div>
          <Badge variant="info" style={badgeMargin}>
            <strong>Reviews: </strong>
          </Badge>{" "}
          {this.reviews(this.state.reviews)}
        </div>
      );
    } else {
      return null;
    }
  };

  tagsLinks = () => {
    if (this.state.reviewed_article_id === "") {
      return (
        <div>
          <Badge variant="info" style={badgeMargin}>
            <strong>Keywords: </strong>
          </Badge>
          {this.tags(this.state.tags)}
          <p />
        </div>
      );
    } else {
      return null;
    }
  };

  versions = () => {
    if (this.state.reviewed_article_id === "") {
      let n = null;
      let p = null;

      if (this.state.next_version !== "") {
        let number_n = parseInt(this.state.article_version) + 1;
        n = (
          <div>
            <Badge variant="info" style={badgeMargin}>
              <strong>Next version: </strong>
            </Badge>
            <Link to={"/article/" + this.state.next_version}>
              <Badge variant="secondary" style={badgeMargin}>
                {number_n}
              </Badge>
            </Link>
            <p />
          </div>
        );
      }

      if (this.state.previous_version !== "") {
        let number_p = parseInt(this.state.article_version) - 1;
        p = (
          <div>
            <Badge variant="info" style={badgeMargin}>
              <strong>Previous version: </strong>
            </Badge>
            <Link to={"/article/" + this.state.previous_version}>
              <Badge variant="secondary" style={badgeMargin}>
                {number_p}
              </Badge>
            </Link>

            <p />
          </div>
        );
      }

      return (
        <div>
          <Badge variant="info" style={badgeMargin}>
            <strong>Version: </strong>
          </Badge>
          <Badge variant="secondary" style={badgeMargin}>
            {this.state.article_version}
          </Badge>
          <p />

          {n}
          {p}
        </div>
      );
    } else {
      return null;
    }
  };

  printComments = () => {
    return this.state.comments.map((comment, id) => (
      <CommentDetails key={id} id={comment} />
    ));
  };

  showButtons = () => {
    const islogged = sessionStorage.getItem("isLogged");
    const id = sessionStorage.getItem("_id");
    if (islogged) {
      if (sessionStorage.getItem("isLogged") === "true") {
        let res = this.state.authors.find(o => {
          return o.id === id;
        });
        if (this.state.reviewed_article_id === "") {
          if (res) {
            if (this.state.next_version === "") {
              return (
                <div>
                  <Button
                    variant="info"
                    style={badgeMargin}
                    as={Link}
                    to={{
                      pathname: "/addversion",
                      state: {
                        previous_version_id: this.state._id,
                        tags: this.state.tags,
                        authors: this.state.authors,
                        article_version: this.state.article_version
                      }
                    }}
                    type=""
                  >
                    Add new version
                  </Button>
                </div>
              );
            } else return null;
          } else {
            return (
              <div>
                <Button
                  variant="info"
                  style={badgeMargin}
                  as={Link}
                  to={{
                    pathname: "/addreview",
                    state: {
                      reviewed_article_id: this.props.match.params.id
                    }
                  }}
                  type=""
                >
                  Add review
                </Button>
                <Button
                  variant="info"
                  style={badgeMargin}
                  as={Link}
                  to={{
                    pathname: "/addcomment",
                    state: {
                      article_id: this.props.match.params.id
                    }
                  }}
                  type=""
                >
                  Add Comment
                </Button>
              </div>
            );
          }
        } else {
          if (res) {
            return null;
          } else {
            return (
              <div>
                <Button
                  variant="info"
                  style={badgeMargin}
                  as={Link}
                  to={{
                    pathname: "/addcomment",
                    state: {
                      article_id: this.props.match.params.id
                    }
                  }}
                  type=""
                >
                  Add Comment
                </Button>
              </div>
            );
          }
        }
      } else {
        return null;
      }
    } else {
      return null;
    }
  };

  render() {
    let title = "";
    if (this.state.reviewed_article_id !== "") {
      title = "Review";
    } else {
      title = "Article";
    }
    return (
      <div>
        <HeaderBar header={title + " Details"} />

        <Card>
          <Card.Header>{this.state.title}</Card.Header>
          <Card.Body>
            <Container className="fluid" style={{ maxWidth: "100%" }}>
              {this.titleReview()}

              <Badge variant="info" style={badgeMargin}>
                <strong>Authors: </strong>
              </Badge>
              {this.authors(this.state.authors)}
              <p />

              {this.tagsLinks()}

              <Badge variant="info" style={badgeMargin}>
                <strong>Abstract: </strong>
              </Badge>
              {this.state.article_abstract}
              <p />

              {this.getAttach()}
              <p />

              {this.versions()}

              <Badge variant="info" style={badgeMargin}>
                <strong>Date of submission: </strong>
              </Badge>
              <Badge variant="secondary">{this.state.date}</Badge>
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Tezos submitter pkh: </strong>
              </Badge>
              <Badge variant="secondary">{this.state.submitter_pkh}</Badge>
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Tezos contract hash: </strong>
              </Badge>
              <Badge variant="secondary">{this.state.hash_contract}</Badge>
              <p />
              {this.reviewsLinks()}
            </Container>
          </Card.Body>
        </Card>
        <p />

        {this.showButtons()}

        <hr />
        <h3>Comments</h3>
        {this.printComments()}
      </div>
    );
  }
}

export default Detail;
