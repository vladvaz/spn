import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";
import AppModals from "./AppModals";
import ReactDOM from "react-dom";
import url from "./ServerIP";

let styleLabel = {
  color: "Black"
};

class AddReview extends Component {
  state = {
    reviewed_article_id: "",
    review_score: "",
    expertise_score: "",
    title: null,
    authors: [],
    tags: [],
    file_name: null,
    file_type: null,
    article_buffer: null,
    hash_contract: "",
    sk: "",
    article_abstract: "",
    article_tags: "",
    date: "",
    user_id: "",
    options: [],
    author: "",
    validated: false,
    message: "",
    show: false
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = message => {
    this.setState({ message: message, show: true });
  };

  componentDidMount() {
    const author = sessionStorage.getItem("name");
    const { reviewed_article_id } = this.props.location.state;
    let newArray = [];
    newArray.push({
      name: author,
      id: sessionStorage.getItem("_id"),
      pkh: sessionStorage.getItem("pkh")
    });
    this.setState({
      reviewed_article_id: reviewed_article_id,
      authors: newArray,
      author: author
    });
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleCheckReviewScore = c => {
    this.setState({
      review_score: c.target.id
    });
  };

  handleCheckExpertiseScore = c => {
    this.setState({
      expertise_score: c.target.id
    });
  };

  handleFile = e => {
    const file = e.target.files[0];
    let reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      const buff = Buffer.from(reader.result);

      this.setState({
        file_name: file.name,
        file_type: file.type,
        article_buffer: buff
      });
      console.log("FILE:", buff);
    };
  };

  upHashBlockChain = () => {
    const eztz = window.eztz;
    eztz.node.setProvider("http://localhost:18731");
    const sk = eztz.crypto.extractKeys(this.state.sk);
    let pkh_authors = this.state.authors.map(
      author => author.id + " : " + author.pkh
    );

    const pair =
      '(Pair "' +
      "FILE_SHA256: " +
      this.state.file_hash +
      '" "' +
      pkh_authors.join(" | ") +
      '")';

    const michel =
      "parameter unit;" +
      "storage (pair string string);" +
      "code { CDR  ;  NIL operation ; PAIR };";

    eztz.contract
      .originate(
        sk,
        0,
        michel,
        pair,
        false,
        false,
        null,
        "010000",
        50000,
        10000
      )
      .then(res => {
        console.log("HASH CONTRACT:", eztz.contract.hash(res.hash, 0));
        this.setState(
          {
            hash_contract: eztz.contract.hash(res.hash, 0)
          },
          () => {
            this.addFileToCouch();
          }
        );
      })
      .catch(err => {
        console.log(err);
        this.handleShow(
          "Communication with Tezos Blockchain failed. Please try again."
        );
      });
  };

  addFileToCouch = () => {
    const obj = {
      title: this.state.title,
      authors: this.state.authors,
      tags: ["Revision"],
      article_abstract: this.state.article_abstract,
      article_version: "1",
      next_version: "",
      previous_version: "",
      reviewed_article_id: this.state.reviewed_article_id,
      reviews: [],
      comments: [],
      submitter_pkh: sessionStorage.getItem("pkh"),
      hash_contract: this.state.hash_contract,
      date: this.state.date,
      file_name: this.state.file_name,
      file_type: this.state.file_type,
      article_buffer: this.state.article_buffer,
      review_score: this.state.review_score,
      expertise_score: this.state.expertise_score
    };
    console.log(obj);

    const headers = {
      spn_token: sessionStorage.getItem("spn_token")
    };

    axios
      .post(url + "/articles/add", obj, { headers })
      .then(res => {
        console.log(res.data);
        this.handleShow("Review Saved.");
        ReactDOM.findDOMNode(this.messageForm).reset();
        this.setState({ validated: false });
      })
      .catch(err => {
        console.log(err.message);
        this.handleShow("Problem during communication. Please try again.");
      });
  };

  getDate = () => {
    const tempDate = new Date();
    const date =
      tempDate.getFullYear() +
      "-" +
      (tempDate.getMonth() + 1) +
      "-" +
      tempDate.getDate() +
      " " +
      tempDate.getHours() +
      ":" +
      tempDate.getMinutes() +
      ":" +
      tempDate.getSeconds();
    this.setState({
      date: date
    });
  };

  generateFileHash = () => {
    let crypto = require("crypto");
    let shasum = crypto.createHash("sha256");
    shasum.update(this.state.article_buffer);
    let file_hash = shasum.digest("hex");
    this.setState({
      file_hash: file_hash
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    e.stopPropagation();

    const form = e.currentTarget.form;
    console.log(form);

    if (form.checkValidity() === true && this.state.authors.length > 0) {
      await this.getDate();
      await this.generateFileHash();
      await this.upHashBlockChain();
      //this.addFileToCouch();
      return;
    }
    this.setState({ validated: true });
  };

  render() {
    return (
      <div>
        <HeaderBar header={"New Review"} />

        <AppModals
          message={this.state.message}
          show={this.state.show}
          handleShow={this.handleShow}
          handleClose={this.handleClose}
        />

        <Form
          noValidate
          validated={this.state.validated}
          id="myForm"
          ref={form => (this.messageForm = form)}
        >
          <Form.Group controlId="article_id">
            <Form.Label style={styleLabel}>Reviewed Article ID</Form.Label>
            <Form.Control
              type="text"
              readOnly
              value={this.state.reviewed_article_id}
            />
          </Form.Group>

          <Form.Group
            controlId="expertise_score"
            onChange={this.handleCheckExpertiseScore}
          >
            <Form.Label style={styleLabel}>
              Your suggested expertise:
            </Form.Label>
            <p />
            <Form.Check
              type="radio"
              label="1"
              name="formRadios1"
              id="1"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="2"
              name="formRadios1"
              id="2"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="3"
              name="formRadios1"
              id="3"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="4"
              name="formRadios1"
              id="4"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="5"
              name="formRadios1"
              id="5"
              inline
              required
            />
          </Form.Group>

          <Form.Group
            controlId="review_score"
            onChange={this.handleCheckReviewScore}
          >
            <Form.Label style={styleLabel}>Your review score:</Form.Label>
            <p />
            <Form.Check
              type="radio"
              label="-2"
              name="formRadios2"
              id="-2"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="-1"
              name="formRadios2"
              id="-1"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="0"
              name="formRadios2"
              id="0"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="1"
              name="formRadios2"
              id="1"
              inline
              required
            />
            <Form.Check
              type="radio"
              label="2"
              name="formRadios2"
              id="2"
              inline
              required
            />
          </Form.Group>

          <Form.Group controlId="title">
            <Form.Label style={styleLabel}>Title</Form.Label>
            <Form.Control
              type="text"
              name="title"
              placeholder="Add a title..."
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input a title!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group>
            <Form.Label style={styleLabel}>Author</Form.Label>
            <Form.Control type="text" readOnly value={this.state.author} />
          </Form.Group>

          <Form.Group controlId="articlePath">
            <Form.Label style={styleLabel}>File</Form.Label>
            <Form.Control
              type="file"
              name="myFile"
              onChange={this.handleFile}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input a file!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="sk">
            <Form.Label style={styleLabel}>Tezos secret key</Form.Label>
            <Form.Control
              type="password"
              name="sk"
              placeholder="Add your Tezos secret key..."
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input your Tezos secret key!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="article_abstract">
            <Form.Label style={styleLabel}>Abstract</Form.Label>
            <Form.Control
              as="textarea"
              rows="5"
              onChange={this.handleChange}
              placeholder="Write an abstract..."
            />
          </Form.Group>

          <Button variant="info" onClick={this.handleSubmit}>
            Upload
          </Button>
        </Form>
      </div>
    );
  }
}

export default AddReview;
