import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class NavBar extends Component {
  state = {
    search_text: "",
    type: "title",
    params: 'title={"keywords":[]}'
  };

  generateParams = () => {
    let keywords = this.state.search_text.trim().split(" ");
    let keywords_final = keywords.filter(s => s.length > 2);
    console.log(keywords_final);
    let params = { keywords: keywords_final };
    let result = this.state.type + "=" + JSON.stringify(params);
    this.setState({
      params: result
    });
  };

  handleChange = e => {
    this.setState(
      {
        search_text: e.target.value
      },
      () => {
        this.generateParams();
      }
    );
  };

  handleType = e => {
    let s = e.target.value.toString().toLowerCase();
    if (s === "keyword") s = "tag";
    this.setState(
      {
        type: s
      },
      () => {
        this.generateParams();
      }
    );
  };

  onKeyPress(event) {
    if (event.which === 13 /* Enter */) {
      event.preventDefault();
    }
  }

  logout = () => {
    sessionStorage.removeItem("_id");
    sessionStorage.removeItem("name");
    sessionStorage.removeItem("pkh");
    sessionStorage.removeItem("spn_token");
    sessionStorage.removeItem("isLogged", false);
    this.props.updateState({ isLogged: false });
  };

  isLoggedIn = () => {
    if (this.props.isLogged) {
      return (
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/">
            My Data
          </Nav.Link>
          <Nav.Link as={Link} to="/addfile">
            New Article
          </Nav.Link>
          <Nav.Link
            as={Link}
            to={{
              pathname: "/listfiles",
              state: {
                id: sessionStorage.getItem("_id"),
                type: "articles"
              }
            }}
          >
            My Articles
          </Nav.Link>

          <Nav.Link
            as={Link}
            to={{
              pathname: "/listfiles",
              state: {
                id: sessionStorage.getItem("_id"),
                type: "reviews"
              }
            }}
          >
            My Reviews
          </Nav.Link>
          <Nav.Link as={Link} to="/" onClick={this.logout}>
            Log out
          </Nav.Link>
        </Nav>
      );
    } else {
      return (
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/">
            Login
          </Nav.Link>
        </Nav>
      );
    }
  };

  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand as={Link} to="/about">
            SPN
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            {this.isLoggedIn()}
            <Form inline onKeyPress={this.onKeyPress}>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                onChange={this.handleChange}
              />

              <Form.Group controlId="type" style={{ marginRight: "6px" }}>
                <Form.Control as="select" onChange={this.handleType}>
                  <option>Title</option>
                  <option>Author</option>
                  <option>Keyword</option>
                </Form.Control>
              </Form.Group>

              <Button
                variant="outline-secondary"
                onClick={this.handeClick}
                as={Link}
                to={"/search/" + this.state.params}
                type=""
              >
                Search
              </Button>
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogged: state.isLogged
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateState: new_state => {
      dispatch({ type: "UPDATE_STATE", new_state: new_state });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);
