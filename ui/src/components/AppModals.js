import React from "react";
import Modal from "react-bootstrap/Modal";

function AppModals(props) {
  return (
    <div>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Information</Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.message}</Modal.Body>
      </Modal>
    </div>
  );
}

export default AppModals;
