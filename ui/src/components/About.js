import React from "react";
import HeaderBar from "./HeaderBar";
import Jumbotron from "react-bootstrap/Jumbotron";

const About = () => {
  return (
    <div>
      <HeaderBar header={"About"} />
      <p />
      <Jumbotron>
        <h1>SPN - Scientific Publishing Network</h1>
        <p />
        This decentralized application aims to track the publications of
        articles and peer reviews. The main difference with the existing
        applications in this area is the use of Tezos blockchain to safeguard
        all the necessary information to validate the author(s) of the article.
      </Jumbotron>

      <p />

      <Jumbotron>
        <h1>This dapp permits to:</h1>
        <p />
        <ul>
          <li>Submmit a new article;</li>
          <li>Review an existing article;</li>
          <li>Comment an existing article;</li>
          <li>View users information.</li>
        </ul>
      </Jumbotron>

      <p />

      <Jumbotron>
        <h1>Developed by:</h1>
        <p />
        <ul>
          <li>Daniel Fernandes - 37197</li>
          <li>Vladimiro Vaz - 37044</li>
        </ul>
      </Jumbotron>

      <HeaderBar header={"Projeto - 2019, UBI"} />
    </div>
  );
};

export default About;
