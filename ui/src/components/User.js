import React, { Component } from "react";
import axios from "axios";
import HeaderBar from "./HeaderBar";
import Container from "react-bootstrap/Container";
import Badge from "react-bootstrap/Badge";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import url from "./ServerIP";

let badgeMargin = {
  marginRight: "4px"
};

class User extends Component {
  state = {
    _id: "",
    name: "",
    pkh: "",
    email: [],
    employment: [],
    articles: [],
    reviews: [],
    n_articles: "",
    n_reviews: ""
  };

  componentDidMount() {
    let id = this.props.match.params.id;

    axios
      .get(url + "/users/" + id)
      .then(response => {
        let n_art = response.data.articles.length;
        let n_rev = response.data.reviews.length;
        this.setState({
          _id: response.data._id,
          name: response.data.name,
          pkh: response.data.pkh,
          email: response.data.email,
          employment: response.data.employment,
          articles: response.data.articles,
          reviews: response.data.reviews,
          n_articles: n_art,
          n_reviews: n_rev
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  emails = e => {
    return e.map((email, i) => {
      return (
        <Badge variant="secondary" key={i} style={badgeMargin}>
          {email}
        </Badge>
      );
    });
  };

  employments = e => {
    return e.map((employment, i) => {
      return (
        <Badge variant="secondary" key={i} style={badgeMargin}>
          {employment}
        </Badge>
      );
    });
  };

  articles = a => {
    return a.map((article, i) => {
      return (
        <Link key={i} to={"/article/" + article}>
          <Badge variant="secondary" style={badgeMargin}>
            {article}
          </Badge>
        </Link>
      );
    });
  };

  reviews = r => {
    return r.map((rev, i) => {
      return (
        <Link key={i} to={"/article/" + rev}>
          <Badge variant="secondary" style={badgeMargin}>
            {rev}
          </Badge>
        </Link>
      );
    });
  };

  render() {
    return (
      <div>
        <HeaderBar header={"User Details"} />

        <Card>
          <Card.Header>{this.state.name}</Card.Header>
          <Card.Body>
            <Container className="fluid" style={{ maxWidth: "100%" }}>
              <Badge variant="info" style={badgeMargin}>
                <strong>Orcid ID: </strong>
              </Badge>
              <Badge variant="secondary" style={badgeMargin}>
                {this.state._id}
              </Badge>
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Tezos public key hash: </strong>
              </Badge>
              <Badge variant="secondary" style={badgeMargin}>
                {this.state.pkh}
              </Badge>
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Email: </strong>
              </Badge>
              {this.emails(this.state.email)}
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Employments: </strong>
              </Badge>
              {this.employments(this.state.employment)}
              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Articles Submitted: </strong>
              </Badge>
              <Badge variant="secondary" style={badgeMargin}>
                {this.state.n_articles}
              </Badge>

              <p />

              <Badge variant="info" style={badgeMargin}>
                <strong>Reviews Submitted: </strong>
              </Badge>
              <Badge variant="secondary" style={badgeMargin}>
                {this.state.n_reviews}
              </Badge>
              <p />

              <Button
                size="sm"
                variant="info"
                as={Link}
                to={{
                  pathname: "/listfiles",
                  state: {
                    id: this.state._id,
                    type: "articles"
                  }
                }}
                type=""
                style={badgeMargin}
              >
                List Articles
              </Button>

              <Button
                size="sm"
                variant="info"
                as={Link}
                to={{
                  pathname: "/listfiles",
                  state: {
                    id: this.state._id,
                    type: "reviews"
                  }
                }}
                type=""
                style={badgeMargin}
              >
                List Reviews
              </Button>
            </Container>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default User;
