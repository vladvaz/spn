# **Este repositório contêm o código da `UI` e o código da `API` do projecto SPN - Scientific Publishing Network**

## Instruções

```
git clone https://gitlab.com/vladvaz/spn.git
cd spn
```

As instruções para correr a `API` e a `UI` encontram-se nas respectivas pastas.
